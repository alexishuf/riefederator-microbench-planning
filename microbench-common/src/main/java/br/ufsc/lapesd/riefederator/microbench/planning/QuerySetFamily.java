package br.ufsc.lapesd.riefederator.microbench.planning;

import javax.annotation.Nonnull;

public enum QuerySetFamily {
    LRB,
    BSBM;

    public static boolean isFamilyName(@Nonnull String querySetName) {
        return querySetName.equals("LRB") || querySetName.equals("BSBM");
    }

    public static QuerySetFamily fromQuerySetName(@Nonnull String querySetName) {
        return querySetName.matches("LRB|[SBC]\\d+") ? LRB : BSBM;
    }
}
