package br.ufsc.lapesd.riefederator.microbench.planning;


import com.google.common.base.Splitter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.nio.file.Files;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.google.common.base.Preconditions.checkState;

public class TempDir implements Closeable {
    private @Nonnull final File tempDir;

    public TempDir() throws IOException {
        tempDir = Files.createTempDirectory("riefederator").toFile();
    }

    public @Nonnull File getFile(@Nonnull String path) {
        List<String> segments = Splitter.on('/').omitEmptyStrings().splitToList(path);
        File current = new File(tempDir.getAbsolutePath());
        for (String segment : segments)
            current = new File(current, segment);
        return current;
    }

    public @Nonnull File extractZipResource(@Nonnull Class<?> aClass,
                                   @Nonnull String relResourcePath) throws IOException {
        return extractZipResource(aClass, relResourcePath, null);
    }
    public @Nonnull File extractZipResource(@Nonnull Class<?> aClass, @Nonnull String relResourcePath,
                                   @Nullable String destinationDir) throws IOException {
        try (InputStream stream = aClass.getResourceAsStream(relResourcePath)) {
            if (stream == null)
                throw new IOException("Resource "+relResourcePath+" not found relative to "+aClass);
            return extractZip(destinationDir == null ? "." : destinationDir, stream);
        }
    }
    public @Nonnull File extractZipResource(@Nonnull String resourcePath) throws IOException {
        return extractZipResource(resourcePath, null);
    }
    public @Nonnull File extractZipResource(@Nonnull String resourcePath,
                                            @Nullable String destinationDir) throws IOException {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = cl.getResourceAsStream(resourcePath)) {
            if (stream == null) throw new IOException("Resource "+resourcePath+" not found");
            return extractZip(destinationDir == null ? "." : destinationDir, stream);
        }
    }

    public @Nonnull File extractZip(@Nonnull String destinationDir,
                                    @Nonnull InputStream stream) throws IOException {
        checkState(tempDir.exists(), "!tempDir.exists()");
        checkState(tempDir.isDirectory(), "!tempDir.isDirectory()");
        File dir = tempDir;
        for (String segment : Splitter.on('/').omitEmptyStrings().splitToList(destinationDir)) {
            dir = new File(dir, segment);
            if (!dir.exists()) {
                if (!dir.mkdirs())
                    throw new IOException("Failed to mkdir"+dir.getAbsolutePath());
            } else if (!dir.isDirectory()) {
                throw new IOException(dir.getAbsolutePath()+" exists but is not a directory");
            }
        }
        assert dir.exists() && dir.isDirectory();

        try (ZipInputStream zs = new ZipInputStream(stream)) {
            for (ZipEntry e = zs.getNextEntry(); e != null; e = zs.getNextEntry()) {
                File file = new File(dir, e.getName());
                if (e.isDirectory()) {
                    if (!file.exists() && !file.mkdirs())
                        throw new IOException("Failed to mkdir directory for entry at "+file);
                } else {
                    File parentFile = file.getParentFile();
                    if (!parentFile.exists() && !parentFile.mkdirs())
                        throw new IOException("Failed to mkdir parent dir for file entry"+file);
                    try (FileOutputStream out = new FileOutputStream(file)) {
                        IOUtils.copy(zs, out);
                    }
                }
            }
        }
        return dir;
    }

    public @Nonnull File extractStream(@Nonnull String destination,
                                       @Nonnull InputStream stream) throws IOException {
        File file = new File(tempDir.getAbsolutePath() + "/" + destination);
        File parent = file.getParentFile();
        if (!parent.exists()) {
            if (!parent.mkdirs())
                throw new IOException("Could not mkdir "+parent.getAbsolutePath());
        }
        try (FileOutputStream out = new FileOutputStream(file)) {
            IOUtils.copy(stream, out);
        }
        return file;
    }

    public @Nonnull File extractResource(@Nonnull Class<?> cls,
                                         @Nonnull String relativePath) throws IOException {
        return extractResource(cls, relativePath, null);
    }
    public @Nonnull File extractResource(@Nonnull Class<?> cls, @Nonnull String relativePath,
                                         @Nullable String destination) throws IOException {
        if (destination == null)
            destination = relativePath.replaceAll("^.*/([^/]+)$", "$1");
        try (InputStream stream = cls.getResourceAsStream(relativePath)) {
            return extractStream(destination, stream);
        }
    }

    public  @Nonnull File extractResource(@Nonnull String resourcePath) throws IOException {
        return extractResource(resourcePath, null);
    }
    public  @Nonnull File extractResource(@Nonnull String resourcePath,
                                          @Nullable String destination) throws IOException {
        if (destination == null)
            destination = resourcePath.replaceAll("^.*/([^/]+)$", "$1");
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try (InputStream in = cl.getResourceAsStream(resourcePath)) {
            if (in == null)
                throw new IOException("Resource "+resourcePath+" not found");
            return extractStream(destination, in);
        }
    }

    @Override public void close() throws IOException {
        if (!tempDir.exists())
            FileUtils.deleteDirectory(tempDir);
    }
}
