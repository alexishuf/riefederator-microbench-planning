#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if ( ! ( sed --version | grep 'GNU sed' >/dev/null ) ); then
  echo "These scripts make extensive use of GNU sed features. Mac OS sed (and likely other implementations) will fail miserably. Use something like homebrew and put a GNU sed in your \$PATH"
  exit 1
fi

if ! java -version &>/dev/null; then
  echo "java is not available!"
  exit 1
fi

usage() {
  echo -e "Usage: $0 [options...] [QUERY_SETS]\n"
  echo    "Options:"
  echo    "    -h  Show this help message\n"
  echo    "    -F  Skip FedX benchmark"
  echo    "    -R  Skip Riefederator benchmark"
  echo    "    -s  Only run the SPARQL parsing benchmark"
  echo    "    -p  Only run the planning (without parsing) benchmark"
  echo    "    -b  Only run the parsing+planning benchmark"
  echo    "    -w  TIME"
  echo    "        Every warmup iteration should take this time. Default: 2s"
  echo    "    -r  TIME"
  echo    "        Every recorded iteration should take this time. Default: 2s"
  echo    "    -f  COUNT"
  echo    "        Pass -f COUNT to JMH executions. JMH will sequentially spawn COUNT"
  echo    "        JVMs where the requested number of warmup and recorded iterations will"
  echo    "        run. Every JVM will only execute a single benchmark method. Default: 1"
  echo    "    -n  COUNT"
  echo    "        Number of executions of the JMH benchmarks. This does not relate to"
  echo    "        JMH -f (forks) option. Under JMH control, results of all forks are "
  echo    "        aggregated into a single mean+CI error result per benchmark method-"
  echo    "        querySetName combination. This option outputs all 5 results of each "
  echo    "        JMH run into the output CSV file. Default: 5"
  echo    "    -c  SECS"
  echo    "        After every JMH execution sleep for SECS seconds, to "
  echo    "        prevent overheating. Default: 5"
  echo    "    -i  COUNT"
  echo    "        Number of measured iterations. Default: 5"
  echo    "    -W  COUNT"
  echo    "        Number of warmup iterations. Default: 5"
  echo    ""
  echo    "QUERY_SETS: A ,-separated list of the following. Default is LRB,BSBM"
  echo    "    ALL               LRB, BSBM and all individual queries of those benchmarks"
  echo    "    LRB               All queries in the LargeRDFBench dataset"
  echo    "    BSBM              All SELECT/ASK queries in the explore set of the"
  echo    "                      Berlin SPARQL Benchmark"
  echo    "    S1,...C10         A query from LargeRDFBench"
  echo    "    query1,...query11 A query from BSBM"
}

# Process options
RUN_FEDX=y
RUN_RIEF=y
METHODS=".*"
JMH_RUNS=5
COOL_SECS=5
FORKS=1
WARM_TIME=2s
RUN_TIME=2s
WARM_RUNS=5
RUN_RUNS=5
QUERY_SETS="LRB,BSBM"
while getopts "hFRspbf:n:w:r:W:i:c:" o; do
  case $o in
    h)
      usage
      exit 0
      ;;
    F)
      RUN_FEDX=n
      ;;
    R)
      RUN_RIEF=n
      ;;
    s)
      METHODS=.parseBenchmark
      ;;
    p)
      METHODS=.planBenchmark
      ;;
    b)
      METHODS=.parseAndPlanBenchmark
      ;;
    f)
      FORKS="$OPTARG"
      ;;
    n)
      JMH_RUNS="$OPTARG"
      ;;
    w)
      WARM_TIME="$OPTARG"
      ;;
    r)
      RUN_TIME="$OPTARG"
      ;;
    i)
      RUN_RUNS="$OPTARG"
      ;;
    W)
      WARM_RUNS="$OPTARG"
      ;;
    c)
      COOL_SECS=$OPTARG
      ;;
  esac
done

# Get QUERY_SETS
ALL_ARGS=( "$0" "$@" )
GIVEN_QUERY_SETS="${ALL_ARGS[$OPTIND]}"
if [ ! -z "$GIVEN_QUERY_SETS" ]; then
  QUERY_SETS="$GIVEN_QUERY_SETS"
fi

# Show parsed options and arguments:
echo "Effective run.sh configuration:"
echo "------------------------------------------"
echo "  RUN_FEDX=$RUN_FEDX"
echo "  RUN_RIEF=$RUN_RIEF"
echo "   METHODS=$METHODS"
echo " WARM_TIME=$WARM_TIME"
echo "  RUN_TIME=$RUN_TIME"
echo " WARM_RUNS=$WARM_RUNS"
echo "  RUN_RUNS=$RUN_RUNS"
echo "     FORKS=$FORKS"
echo "  JMH_RUNS=$JMH_RUNS"
echo " COOL_SECS=$COOL_SECS"
echo "QUERY_SETS=$QUERY_SETS"
echo "------------------------------------------"

# Build
cd "$DIR"
LOG=$(mktemp)
echo "Building... (may take 1~2 minutes)"
if ! time bash -c "./mvnw -DskipTests=true package &> '$LOG' "; then
  cat "$LOG"
  echo "Failed to build benchmark suite"
  exit 1
else
  echo "Built jars."
fi

function run_jmh() {
  JAR="$1"
  VARIANT=$2
  ADD_OPENS="--add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED"
  if ( java -version | grep -E 'version "?1.8' &> /dev/null ) ; then
    ADD_OPENS=""
  fi
  QUERY_SET_OPTS=
  if [ "$QUERY_SETS" != "ALL" ]; then
    QUERY_SET_OPTS="-p querySetName=$QUERY_SETS"
  fi
  cd "$DIR"
  mkdir -p results
  CSV="results/$VARIANT.csv"
  rm -f "$CSV.tmp"
  for i in $(seq 1 $JMH_RUNS) ; do
    rm "$CSV"
    ( set -x ; java $ADD_OPENS -jar "$JAR" -f $FORKS -rf CSV -rff "$CSV" \
       -w $WARM_TIME -r $RUN_TIME -wi $WARM_RUNS -i $RUN_RUNS \
       $QUERY_SET_OPTS ".*PlanningBenchmarks$METHODS" \
       2>&1 | tee "results/$VARIANT.log" ) || exit 1
    if [ -f "$CSV.tmp" ]; then
      tail -n +2 "$CSV" >> "$CSV.tmp"
    else
      cp "$CSV" "$CSV.tmp"
    fi
    ( set -x ; sleep $COOL_SECS )
  done
  mv "$CSV.tmp" "$CSV"
}

if [ "$RUN_FEDX" == y ]; then
  run_jmh "fedx-microbench/target/fedx-microbench-fatjar.jar" fedx
fi

if [ "$RUN_RIEF" == y ]; then
  run_jmh "riefederator-microbench/target/riefederator-microbench-fatjar.jar" riefederator
fi


