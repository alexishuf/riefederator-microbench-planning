package br.ufsc.lapesd.riefederator.microbench.planning;

import com.fluidops.fedx.Config;
import com.fluidops.fedx.FedX;
import com.fluidops.fedx.FedXFactory;
import com.fluidops.fedx.FederationManager;
import com.fluidops.fedx.evaluation.concurrent.ControlledWorkerScheduler;
import com.fluidops.fedx.structures.Endpoint;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.openjdk.jmh.annotations.*;
import org.openrdf.query.*;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.repository.sail.SailRepositoryConnection;
import org.openrdf.repository.sail.SailTupleQuery;
import org.openrdf.sail.SailConnection;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkState;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.stream;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.openrdf.query.QueryLanguage.SPARQL;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Thread)
public class PlanningBenchmarks {
    private static final @Nonnull BindingSet emptyBindingSet = EmptyBindingSet.getInstance();

    @Param({"LRB",
            "B1",
            "B2",
            "B3",
            "B4",
            "B5",
            "B6",
            "B7",
            "B8",
            "C1",
            "C10",
            "C2",
            "C3",
            "C4",
            "C5",
            "C6",
            "C7",
            "C8",
            "C9",
            "S1",
            "S10",
            "S11",
            "S12",
            "S13",
            "S14",
            "S2",
            "S3",
            "S4",
            "S5",
            "S6",
            "S7",
            "S8",
            "S9",
            "BSBM",
            "query1",
            "query2",
            "query3",
            "query4", /* UNION */
            "query5",
            "query6", /* REGEX */
            "query7",  /* uses Product,Offer,Vendor,Review  & Person */
            "query8",
            "query10",
            "query11" /*UNION & unbound predicate */
    })
    private String querySetName;

    private TempDir tmp;
    private SailRepository federation;
    private final List<String> sparqlQueries = new ArrayList<>();
    private final List<TupleQuery> parsedQueries = new ArrayList<>();
    private SailRepositoryConnection federationConnection;
    private final List<TupleQuery> benchmarkParsed = new ArrayList<>();
    private final List<TupleQueryResult> benchmarkPlanned = new ArrayList<>();

    @Setup(Level.Trial)
    public void setUp() throws Exception {
        checkState(tmp == null);

        tmp = new TempDir();
        QuerySetFamily family = QuerySetFamily.fromQuerySetName(querySetName);
        if (family == QuerySetFamily.LRB) {
            tmp.extractZipResource(PlanningBenchmarks.class, "lrb-queries.zip");
            tmp.extractZipResource(PlanningBenchmarks.class, "fedx-lrb.zip");

        } else {
            assert family == QuerySetFamily.BSBM;
            tmp.extractZipResource(PlanningBenchmarks.class, "bsbm-queries.zip");
            tmp.extractZipResource(PlanningBenchmarks.class, "fedx-bsbm.zip");
        }
        Config.initialize();
        Config cfg = Config.getConfig();
        cfg.set("validateRepositoryConnections", "false");
        cfg.set("cacheLocation", tmp.getFile("cache.db").getAbsolutePath());
        List<String> endpoints = parseEndpoints();
        federation = FedXFactory.initializeSparqlFederation(endpoints);
        federationConnection = federation.getConnection();
        sparqlQueries.clear();
        parsedQueries.clear();
        for (String name : loadQueryNames()) {
            try (FileInputStream in = new FileInputStream(tmp.getFile("queries/" + name))) {
                String sparql = IOUtils.toString(in, UTF_8);
                sparqlQueries.add(sparql);
                parsedQueries.add(federationConnection.prepareTupleQuery(SPARQL, sparql));
            }
        }
    }

    @TearDown(Level.Trial)
    public void tearDown() throws Exception {
        FedX fedX = (FedX)this.federation.getSail();
        SailRepositoryConnection federationConnection = this.federationConnection;
        this.federationConnection = null;
        this.federation = null;

        federationConnection.close();
        FederationManager fedMgr = FederationManager.getInstance();
        ControlledWorkerScheduler<BindingSet> joinScheduler = fedMgr.getJoinScheduler();
        ControlledWorkerScheduler<BindingSet> unionScheduler = fedMgr.getUnionScheduler();
        List<Endpoint> endpoints = fedX.getMembers();
        ExecutorService ex = (ExecutorService) fedMgr.getExecutor();
        fedMgr.shutDown();
        joinScheduler.abort();
        unionScheduler.abort();
        for (Endpoint ep : endpoints)
            ep.shutDown();
        ex.shutdown();
        if (!ex.awaitTermination(1, TimeUnit.SECONDS)) {
            System.err.println("awaitTermination() on FederationManager executor timed out. " +
                               "This a known bug");
        }

        try {
            // will not fix synchronization issues, but prevents overheating
            Thread.sleep(5000);
        } catch (InterruptedException ignored) { }
        tmp.close();
    }

    private @Nonnull List<String> loadQueryNames() {
        List<String> queryNames;
        if (QuerySetFamily.isFamilyName(querySetName)) {
            File[] files = tmp.getFile("queries").listFiles();
            queryNames = stream(files).map(File::getName).sorted().collect(toList());
        } else {
            queryNames = singletonList(querySetName);
        }
        return queryNames;
    }

    @Nonnull private List<String> parseEndpoints() throws IOException {
        List<String> endpoints;
        try (FileInputStream in = new FileInputStream(tmp.getFile("endpoints"))) {
            endpoints = IOUtils.readLines(in, UTF_8);
        }
        for (ListIterator<String> it = endpoints.listIterator(); it.hasNext(); ) {
            String trimmed = it.next().trim();
            if (trimmed.isEmpty()) it.remove();
            else                   it.set(trimmed);
        }
        return endpoints;
    }

    @Benchmark
    public @Nonnull List<TupleQuery> parseBenchmark() throws Exception {
        benchmarkParsed.clear();
        for (String sparql : sparqlQueries) {
            benchmarkParsed.add(federationConnection.prepareTupleQuery(SPARQL, sparql));
        }
        return benchmarkParsed;
    }

    @Benchmark
    public @Nonnull List<TupleQueryResult> planBenchmark() throws Exception {
        benchmarkPlanned.clear();
        for (TupleQuery query : parsedQueries) {
            TupleQueryResult result = query.evaluate();
            benchmarkPlanned.add(result);
            result.close();
        }
        return benchmarkPlanned;
    }

    @Benchmark
    public @Nonnull List<TupleQueryResult> parseAndPlanBenchmark() throws Exception {
        benchmarkPlanned.clear();
        for (String sparql : sparqlQueries) {
            TupleQuery query = federationConnection.prepareTupleQuery(SPARQL, sparql);
            TupleQueryResult results = query.evaluate();
            benchmarkPlanned.add(results);
            results.close();
        }

        return benchmarkPlanned;
    }
}
