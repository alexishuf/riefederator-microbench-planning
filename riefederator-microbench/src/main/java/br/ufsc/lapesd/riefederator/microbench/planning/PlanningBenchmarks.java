package br.ufsc.lapesd.riefederator.microbench.planning;

import br.ufsc.lapesd.riefederator.algebra.Op;
import br.ufsc.lapesd.riefederator.federation.Federation;
import br.ufsc.lapesd.riefederator.federation.spec.FederationSpecLoader;
import br.ufsc.lapesd.riefederator.query.parse.SPARQLParseException;
import br.ufsc.lapesd.riefederator.query.parse.SPARQLParser;
import org.apache.commons.io.IOUtils;
import org.openjdk.jmh.annotations.*;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkState;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toList;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Thread)
public class PlanningBenchmarks {
    @Param({"LRB",
            "B1",
            "B2",
            "B3",
            "B4",
            "B5",
            "B6",
            "B7",
            "B8",
            "C1",
            "C10",
            "C2",
            "C3",
            "C4",
            "C5",
            "C6",
            "C7",
            "C8",
            "C9",
            "S1",
            "S10",
            "S11",
            "S12",
            "S13",
            "S14",
            "S2",
            "S3",
            "S4",
            "S5",
            "S6",
            "S7",
            "S8",
            "S9",
            "BSBM",
            "query1",
            "query2",
            "query3",
            "query4", /* UNION */
            "query5",
            "query6", /* REGEX */
            "query7",  /* uses Product,Offer,Vendor,Review  & Person */
            "query8",
            "query10",
            "query11" /*UNION & unbound predicate */
    })
    private String querySetName;

    private TempDir tmp;
    private Federation federation;
    private final @Nonnull List<String> sparqlQueries = new ArrayList<>();
    private final @Nonnull List<Op> parsedQueries = new ArrayList<>();
    private final @Nonnull List<Op> results = new ArrayList<>();

    @Setup(Level.Trial)
    public void setUp() throws Exception {
        checkState(tmp == null);
        checkState(federation == null);

        tmp = new TempDir();
        QuerySetFamily queryFamily = QuerySetFamily.fromQuerySetName(querySetName);
        if (queryFamily == QuerySetFamily.LRB) {
            tmp.extractZipResource(PlanningBenchmarks.class, "riefederator-lrb.zip");
            tmp.extractZipResource(PlanningBenchmarks.class, "lrb-queries.zip");
        } else {
            assert queryFamily == QuerySetFamily.BSBM;
            tmp.extractZipResource(PlanningBenchmarks.class, "riefederator-bsbm.zip");
            tmp.extractZipResource(PlanningBenchmarks.class, "bsbm-queries.zip");
        }
        federation = new FederationSpecLoader().load(tmp.getFile("riefederator/config.yaml"));
        List<String> queryNames;
        if (QuerySetFamily.isFamilyName(querySetName)) {
            queryNames = Arrays.stream(tmp.getFile("queries").listFiles())
                                .map(File::getName).sorted().collect(toList());
        } else {
            queryNames = Collections.singletonList(querySetName);
        }
        sparqlQueries.clear();
        parsedQueries.clear();
        for (String name : queryNames) {
            try (FileInputStream in = new FileInputStream(tmp.getFile("queries/" + name))) {
                String sparql = IOUtils.toString(in, UTF_8);
                sparqlQueries.add(sparql);
                parsedQueries.add(SPARQLParser.tolerant().parse(sparql));
            }
        }
    }

    @TearDown(Level.Trial)
    public void tearDown() throws IOException {
        checkState(federation != null);
        checkState(tmp != null);
        federation.close();
        federation = null;
        try {
            //unecessary for synchronization, but prevents overheating
            Thread.sleep(5000);
        } catch (InterruptedException ignored) { }
        tmp.close();
        tmp = null;
    }

    @Benchmark
    public @Nonnull List<Op> parseBenchmark() throws SPARQLParseException {
        SPARQLParser parser = SPARQLParser.tolerant();
        results.clear();
        for (String sparql : sparqlQueries)
            results.add(parser.parse(sparql));
        return results;
    }

    @Benchmark
    public @Nonnull List<Op> planBenchmark() {
        results.clear();
        for (Op query : parsedQueries)
            results.add(federation.plan(query));
        return results;
    }

    @Benchmark
    public @Nonnull List<Op> parseAndPlanBenchmark() throws SPARQLParseException {
        SPARQLParser parser = SPARQLParser.tolerant();
        results.clear();
        for (String sparql : sparqlQueries)
            results.add(federation.plan(parser.parse(sparql)));
        return results;
    }
}
